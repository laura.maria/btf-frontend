FROM node:11

COPY . /srv
WORKDIR /srv
RUN npm install
RUN npm run build

CMD npm start
