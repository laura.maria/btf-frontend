#!/bin/sh

sed -i "s/\$IMAGE_TAG/$1/g" $(pwd)/deploy/k8s/*

kubectl apply -f ./deploy/k8s --record
