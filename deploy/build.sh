!#!/bin/sh

docker pull $2:latest || true
docker image build --cache-from $2 --pull . -t $1 -t $2:latest