export const resultOptions = [
  { key: 'WIN', text: 'Win', value: 'WIN' },
  { key: 'LOSE', text: 'Lose', value: 'LOSE' },
  { key: 'TIE', text: 'Tie', value: 'TIE' }
]

export const roundOptions = [
  { key: 'ONE', text: '1 Runda', value: 'ONE' },
  { key: 'TWO', text: '2 Runde', value: 'TWO' },
  { key: 'THREE', text: '3 Runde', value: 'THREE' }
]

export const methodOptions = [
  { key: 'KO/TKO', text: 'KO/TKO', value: 'KO/TKO' },
  { key: 'POINTS', text: 'Points', value: 'POINTS' },
  { key: 'UNANIMITY', text: 'Unanimity', value: 'UNANIMITY' }
]
