// let host = window.location.host.split('iam.').join('').split('api').join('')
//
// if (host.indexOf('localhost') > -1) host = 'borntofight.live/api'

// export const baseUrl = 'https://api.' + host
// export const keycloakUrl = 'https://iam.' + host

// const realm = 'PeerHelper'
// export const redirectUrl = (window.location.host.indexOf('localhost') > -1 ? 'http://' : 'https://') + window.location.host + '/login'
// export const keycloack = `${keycloakUrl}/auth/realms/${realm}/protocol/openid-connect/auth?scope=openid&response_type=code&client_id=api&redirect_uri=${redirectUrl}`
// export const keycloackCodeLogin = `${keycloakUrl}/auth/realms/${realm}/protocol/openid-connect/token`
// export const keycloakToken = baseUrl.indexOf('peerhelper.live') > -1 ? 'YXBpOjBkNTYwODk3LTI3MzYtNDhjMy04ZGIyLWUxZWQ1MWQ1YjgyNg==' : `YXBpOjQ0NzdlYTg3LWE5OGEtNGY3My04MDM1LWZkNzBmNWQwMjAxYg==`

export const baseUrl = 'http://api.borntofight.live/api'

const fighters = baseUrl + '/fighters'

const events = baseUrl + '/events'

const me = 'http://borntofight.live/api' + '/me'

const contact = baseUrl + '/contact'

const search = {
  events: baseUrl + '/search/events',
  fighters: baseUrl +'/search/fighters'
}


export default { fighters, events, me, contact, search }
