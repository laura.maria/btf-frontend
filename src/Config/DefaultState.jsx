import * as roundedPicture from "../Assets/Images/circleImage.png";
import * as roundedPicture2 from '../Assets/Images/profileImage.jpg'
import * as roundedPicture3 from '../Assets/Images/prrofileImage.jpg'
import * as wallPicture from '../Assets/Images/georgeFlorescu.png'
import * as eventPicture from '../Assets/Images/georgeFlorescu.png'
import * as eventPicture2 from '../Assets/Images/eventShow.png'

export const requestStatus = 'INITIAL'

export const myId = null

export const fighters = []
export const mockFighters = [
  {
    id: 1,
    firstName: 'George',
    lastName: 'Florescu',
    email: '',
    age: 22,
    profilePicture: roundedPicture,
    wallPicture: wallPicture,
    country: 'Romania',
    gloryRecord: '61-11-1 (35 KO)',
    description: 'George Florescu este un kickboxer român și fost campion mondial SUPERKOMBAT la categoria cruiser. Este de asemenea și cel mai tânăr campion din istoria promoției.\n' +
      '\n' +
      '                Este semnat și de către GLORY acolo unde pe 21 iunie 2014 la Los Angeles a luptat cu unul dintre favoriții marelui turneu al mijlociilor, piramida "Last Man Standing" - americanul Wayne Barrett - și a pierdut prin knockout în runda a treia după un meci relativ egal până la acel moment.\n' +
      '\n' +
      '                Începând cu data de 1 septembrie 2016, este clasat pe locul 10 în clasamentul mondial al Combat Press.\n' +
      '\n' +
      '                Este fost dublu campion național de wushu',
    phone : '',
    club: '',
    couch: '',
    nickname: 'FG',
    height: 178,
    weight: 69.4,
    category: 'Usoara',
    place: 'Campion categoria usoara',
    fightTime: '8:31',
    knockdown: '10:0',
    precisionz: '49.39%',
    slpm: '13:66',
    sapm: '10:89',
    difference: '2.77',
    facebook: '-',
    instagram: '-',
    youtube: '-',
    website: '-'
  },
  {
    id: 2,
    firstName: 'Ion',
    lastName: 'Popescu',
    email: '',
    age: 24,
    profilePicture: roundedPicture2,
    wallPicture: wallPicture,
    country: 'ROMANIA',
    gloryRecord: '12-5-1 (5 KO)',
    description: 'Ion Popescu este un kickboxer român și fost campion mondial SUPERKOMBAT la categoria cruiser. Este de asemenea și cel mai tânăr campion din istoria promoției.\n' +
      '\n' +
      '                Este semnat și de către GLORY acolo unde pe 21 iunie 2014 la Los Angeles a luptat cu unul dintre favoriții marelui turneu al mijlociilor, piramida "Last Man Standing" - americanul Wayne Barrett - și a pierdut prin knockout în runda a treia după un meci relativ egal până la acel moment.\n' +
      '\n' +
      '                Începând cu data de 1 septembrie 2016, este clasat pe locul 10 în clasamentul mondial al Combat Press.\n' +
      '\n' +
      '                Este fost dublu campion național de wushu',
    phone : '',
    club: '',
    couch: '',
    nickname: 'Pop',
    height: 175,
    weight: 70.4,
    category: 'Usoara',
    place: 'Campion categoria usoara',
    fightTime: '8:31',
    knockdown: '10:0',
    precisionz: '49.39%',
    slpm: '13:66',
    sapm: '10:89',
    difference: '2.77',
    facebook: '-',
    instagram: '-',
    youtube: '-',
    website: '-'
  },
  {
    id: 3,
    firstName: 'Ivan',
    lastName: 'Antoniu',
    email: '',
    age: 26,
    profilePicture: roundedPicture3,
    wallPicture: wallPicture,
    country: 'ROMANIA',
    gloryRecord: '31-10-2 (15 KO)',
    description: 'Ivan Antoniu este un kickboxer român și fost campion mondial SUPERKOMBAT la categoria cruiser. Este de asemenea și cel mai tânăr campion din istoria promoției.\n' +
      '\n' +
      '                Este semnat și de către GLORY acolo unde pe 21 iunie 2014 la Los Angeles a luptat cu unul dintre favoriții marelui turneu al mijlociilor, piramida "Last Man Standing" - americanul Wayne Barrett - și a pierdut prin knockout în runda a treia după un meci relativ egal până la acel moment.\n' +
      '\n' +
      '                Începând cu data de 1 septembrie 2016, este clasat pe locul 10 în clasamentul mondial al Combat Press.\n' +
      '\n' +
      '                Este fost dublu campion național de wushu',
    phone : '',
    club: '',
    couch: '',
    nickname: 'Ivanny',
    height: 180,
    weight: 90.4,
    category: 'Grea',
    place: 'Campion categoria grea',
    fightTime: '8:31',
    knockdown: '10:0',
    precisionz: '49.39%',
    slpm: '13:66',
    sapm: '10:89',
    difference: '2.77',
    facebook: '-',
    instagram: '-',
    youtube: '-',
    website: '-'
  }
]
export const events = []
export const mockEvents = [
  {
    id: 1,
    eventPicture: eventPicture,
    organizationName: 'Super Kombat',
    date: '21/06/2020',
    time: '19:00',
    city: 'Arad',
    address: 'Sala polivalenta',
    tickets: '-',
    description: ''
  },
  {
    id: 2,
    eventPicture: eventPicture2,
    organizationName: 'Dynamite Fighting Show',
    date: '05/07/2020',
    time: '20:00',
    city: 'Bucuresti',
    address: 'Sala polivalenta',
    tickets: 'www.eventim.ro',
    description: ''
  },
  {
    id: 3,
    eventPicture: eventPicture,
    organizationName: 'Super Kombat',
    date: '26/08/2020',
    time: '19:30',
    city: 'Bucuresti',
    address: 'Romexpo',
    tickets: '0745635243',
    description: ''
  }
]

export const editedFighter = {
  firstName: '',
  lastName: '',
  email: '',
  age: 0,
  profilePicture: null,
  wallPicture: null,
  country: '',
  gloryRecord: '',
  description: '',
  phone : '',
  club: '',
  couch: '',
  nickname: '',
  height: 0,
  weight: 0,
  category: '',
  place: '',
  fightTime: '',
  knockdown: '',
  precisionz: '',
  slpm: '',
  sapm: '',
  difference: '',
  facebook: '',
  instagram: '',
  youtube: '',
  website: ''
}
export const editedEvent = {
  id: 1,
  eventPicture: eventPicture,
  organizationName: 'Super Kombat',
  date: '21/06/2020',
  time: '19:00',
  city: 'Arad',
  address: 'Sala polivalenta',
  tickets: '-',
  description: 'descriere descriere descriere descriere descriere descriere descriere descriere descriere descriere descriere descriere' +
    'descriere descriere descriere descriere descriere descriere descriere descriere descriere' +
    'descriere descriere descriere descriere descriere descriere descriere descriere descriere' +
    ' descriere descriere descriere descriere descriere descriere descriere descriere descriere '
}

export const mockFights = [
  {
    id: 1,
    opponent: 'Popescu Ion',
    fightDate: '02/08/2019',
    event: 'Glory: Dusseldorf',
    win: 'LOSE',
    rounds: 'THREE',
    time: '4:21',
    method: 'KO/TKO',
    videoLink: '-'
  },
  {
    id: 2,
    opponent: 'Antoniu',
    fightDate: '14/04/2019',
    event: 'Super Kombat',
    win: 'LOSE',
    rounds: 'THREE',
    time: '4:15',
    method: 'KO/TKO',
    videoLink: '-'
  },
  {
    id: 3,
    opponent: 'Popescu Ion',
    fightDate: '01/11/2019',
    event: 'Super Kombat',
    win: 'WIN',
    rounds: 'THREE',
    time: '3:41',
    method: 'KO/TKO',
    videoLink: '-'
  }
]

export const editedFight = {
  opponent: '',
  fightDate: '',
  event: '',
  win: '',
  rounds: '',
  time: '',
  method: '',
  videoLink: ''
}


// {
//   "firstName": "George",
//   "lastName": "Florescu",
//   "email": "",
//   "age": 22,
//   "profilePicture": null,
//   "wallPicture": null,
//   "country": "Romania",
//   "gloryRecord": "61-11-1 (35 KO)",
//   "description": "George Florescu este un kickboxer român și fost campion mondial SUPERKOMBAT la categoria cruiser. Este de asemenea și cel mai tânăr campion din istoria promoției.              Este semnat și de către GLORY acolo unde pe 21 iunie 2014 la Los Angeles a luptat cu unul dintre favoriții marelui turneu al mijlociilor, piramida Last Man Standing - americanul Wayne Barrett - și a pierdut prin knockout în runda a treia după un meci relativ egal până la acel moment.      Începând cu data de 1 septembrie 2016, este clasat pe locul 10 în clasamentul mondial al Combat Press.      Este fost dublu campion național de wushu",
//   "phone": "",
//   "club": "",
//   "couch": "",
//   "nickname": "FG",
//   "height": 178,
//   "weight": 69.4,
//   "category": "Usoara",
//   "place": "Campion categoria usoara",
//   "fightTime": "8:31",
//   "knockdown": "10:0",
//   "precisionz": "49.39%",
//   "slpm": "13:66",
//   "sapm": "10:89",
//   "difference": "2.77",
//   "facebook": "-",
//   "instagram": "-",
//   "youtube": "-",
//   "website": "-"
// }

