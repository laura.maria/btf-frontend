import React from 'react'
import ReactDOM from 'react-dom'
import {createBrowserHistory} from 'history'
import {CookiesProvider} from 'react-cookie'
import {Route, Router, Switch} from 'react-router-dom'
import routes from './Routes/index.jsx'
import 'semantic-ui-css/semantic.min.css'
import AppMenu from './Components/AppMenu'
import './index.css'
import Footer from "./Components/Footer";
import {MuiPickersUtilsProvider} from '@material-ui/pickers'
import MomentUtils from '@date-io/moment'
import '../src/Components/i18n'

const history = createBrowserHistory()

const App = () => {

  return (
    <div style={{minHeight: '100vh'}}>
      <CookiesProvider>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <Router history={history}>
            <AppMenu history={history}>
              <Switch>
                {routes.map((prop, key) => <Route path={prop.path} component={prop.component} key={key}/>)}
              </Switch>
            </AppMenu>
            <Footer/>
          </Router>
        </MuiPickersUtilsProvider>
      </CookiesProvider>
    </div>
  )
}

ReactDOM.render(<App/>, document.getElementById('root'))
