import axios from 'axios'
import endpoints from '../Config/ApiEndpoints'
import { processRequestErrorNotification } from '../Utils/Error'


export const getEventsSearch = async (store, filter) => {

  let url = endpoints.search.events + '/' + filter
  store.setState({ requestStatus: 'LOADING' })
  try {
    const response = await axios.get(url)
    const  events  = response.data


    store.setState({ requestStatus: 'SUCCESS', events })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ events: [], requestStatus: 'FAILED', notification })
  }
}


export const getFightersSearch = async (store, filter) => {

  let url = endpoints.search.fighters + '/' + filter
  store.setState({ requestStatus: 'LOADING' })
  try {
    const response = await axios.get(url)
    const fighters  = response.data


    store.setState({ requestStatus: 'SUCCESS', fighters })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ fighters: [], requestStatus: 'FAILED', notification })
  }
}
