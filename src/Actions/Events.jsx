import axios from 'axios'
import endpoints from '../Config/ApiEndpoints'
import { processRequestErrorNotification } from '../Utils/Error'
import { editedEvent as defaultEditedEvent, mockEvents } from '../Config/DefaultState'


export const getEvents = async (store) => {

  let url = endpoints.events
  store.setState({ requestStatus: 'LOADING' })
  try {
    const response = await axios.get(url)
    const { events } = response.data._embedded
    //  const events = mockEvents
    // console.log('getEvents', events)

    store.setState({ requestStatus: 'SUCCESS', events })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ fighters: mockEvents, requestStatus: 'FAILED', notification })
  }
}

export const getEvent = async (store, eventId) => {

  let url = endpoints.events
  store.setState({ requestStatus: 'LOADING' })

  try {
    const response = await axios.get(url + '/' + eventId)
    const editedEvent = response.data

    store.setState({ requestStatus: 'SUCCESS', editedEvent })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ editedFighter: defaultEditedEvent, requestStatus: 'FAILED' })
  }
}


export const saveEvent = async (store, event, eventId) => {

  let url = endpoints.events
  store.setState({ requestStatus: 'LOADING' })
  console.log('saveEvent', eventId, event)
  try {
    if (eventId === '0') {
      await axios.post(url, event)
    } else {
      await axios.patch(url + '/' + event.id, event)
    }

  } catch (error) {
    store.setState({ requestStatus: 'FAILED' })
  }
}
