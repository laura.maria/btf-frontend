import endpoints from "../Config/ApiEndpoints";
import axios from "axios";

export const saveContact = async (store, contact) => {

  let url = endpoints.contact
  store.setState({ requestStatus: 'LOADING' })

  try {
    await axios.post(url, contact)
  } catch (error) {
    store.setState({ requestStatus: 'FAILED' })
  }
}
