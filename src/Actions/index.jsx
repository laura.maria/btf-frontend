import * as fighters from './Fighters'
import * as events from './Events'
import * as contact from './Contact'
import * as search from './Search'

export default {
  fighters,
  events,
  contact,
  search
}
