import axios from 'axios'
import endpoints from '../Config/ApiEndpoints'
import { processRequestErrorNotification } from '../Utils/Error'
import { editedFighter as defaultEditedFighter, mockFighters, mockFights, editedFight as defaultEditedFight } from '../Config/DefaultState'


export const getMyId = async (store) => {

  let url = endpoints.me
  store.setState({ requestStatus: 'LOADING' })
  try {
    const response = await axios.get(url)
    console.log('response myId', response)
    const myId  = response.data


    store.setState({ requestStatus: 'SUCCESS', myId })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ myId: null, requestStatus: 'FAILED', notification })
  }
}

export const getFighters = async (store) => {

  let url = endpoints.fighters
  store.setState({ requestStatus: 'LOADING' })
  try {
    const response = await axios.get(url)
    const { fighters } = response.data._embedded


    store.setState({ requestStatus: 'SUCCESS', fighters })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ fighters: mockFighters, requestStatus: 'FAILED', notification })
  }
}

export const getFighter = async (store, fighterId) => {

  let url = endpoints.fighters
  store.setState({ requestStatus: 'LOADING' })

  try {
    const response = await axios.get(url + '/' + fighterId)
    const editedFighter = response.data

    store.setState({ requestStatus: 'SUCCESS', editedFighter })
  } catch (error) {
    store.setState({ editedFighter: defaultEditedFighter, requestStatus: 'FAILED' })
  }
}


export const saveFighter = async (store, fighter, fighterId) => {

  let url = endpoints.fighters
  store.setState({ requestStatus: 'LOADING' })
  console.log('saveFighter', fighterId, fighter)
  try {
    if (fighterId === '0') {
      await axios.post(url, fighter)
    } else {
      await axios.post(url, {...fighter, id: fighterId})
    }

  } catch (error) {
    store.setState({ requestStatus: 'FAILED' })
  }
}


export const getFighterFights = async (store, fighterId) => {

  let url = endpoints.fighters
  store.setState({ requestStatus: 'LOADING' })

  try {
    const response = await axios.get(url + '/' + fighterId + '/fights')
    const fighterFights = response.data._embedded.fights

    store.setState({ requestStatus: 'SUCCESS', fighterFights })
  } catch (error) {
    store.setState({ fighterFights: mockFights, requestStatus: 'FAILED' })
  }
}

export const getFight = async (store, id) => {

  let url = 'http://api.borntofight.live/api/fights'
  store.setState({ requestStatus: 'LOADING' })

  try {
    const response = await axios.get(url + '/' + id)
    const editedFight = response.data

    store.setState({ requestStatus: 'SUCCESS', editedFight })
  } catch (error) {
    store.setState({ editedFight: defaultEditedFighter, requestStatus: 'FAILED' })
  }
}

export const saveFight = async (store, fight, fighterId, fightId) => {

  let url = endpoints.fighters + '/' + fighterId + '/fights'
  store.setState({ requestStatus: 'LOADING' })
  console.log('saveFight', fighterId, fight)
  try {
    if (fightId !== '0') {
      await axios.patch('http://api.borntofight.live/api/fights/' + fightId, fight)
    } else {
      await axios.post(url, fight)
    }

  } catch (error) {
    store.setState({ requestStatus: 'FAILED' })
  }
}

export const deleteFight = async (store, fightId) => {

  store.setState({ requestStatus: 'LOADING' })

  try {
    await axios.delete('http://api.borntofight.live/api/fights/' + fightId)

    store.setState({ requestStatus: 'SUCCESS' })
  } catch (error) {
    const notification = processRequestErrorNotification(error)
    store.setState({ requestStatus: 'FAILED', notification })
  }
}



