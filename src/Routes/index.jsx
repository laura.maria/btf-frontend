
import Fighters from '../Pages/Fighters'
import Events from '../Pages/Events'
import Fighter from '../Pages/Fighters/Fighter'
import Profile from '../Pages/Profile'
import Records from '../Pages/Records'
import Contact from '../Pages/Contact'
import Event from '../Pages/Events/Event'
import EventView from "../Components/EventView";
import React  from 'react'


const routes = [

  { path: '/events', name: 'Events', component: Events },
  { path: '/fighters/:id', name: 'Fighter', component: Fighter },
  { path: '/fighters', name: 'Fighters', component: Fighters },
  { path: '/my-profile/:id?', name: 'My Profile', component: Profile },
  { path: '/event/view/:id', name: 'Event', component: EventView},
  { path: '/event/:id', name: 'Event', component: Event},
  { path: '/contact', name: 'Contact', component: Contact },
  { path: '/records/:id?/:extraId?', name: 'My Records', component: Records },
  { path: '/:section?/:id?/:action?/:extraId?', name: 'HomePage', component: Events }
]

export default routes
