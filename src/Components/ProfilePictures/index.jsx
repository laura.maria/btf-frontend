import React, {useState, useEffect} from 'react'
import {
  Icon,
  Responsive,
  Table,
  Pagination,
  Image,
  Grid,
  Button,
  Item,
  Header,
  Flag,
  Divider,
  Segment, Form
} from "semantic-ui-react";
import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import {Link, NavLink} from "react-router-dom";
import * as roundedPicture from "../../Assets/Images/circleImage.png";
import * as coverPicture from '../../Assets/Images/georgeFlorescu.png';
import {Trans} from "react-i18next";
import ImageUploader from 'react-images-upload';
import axios from "axios";


const ProfilePictures = (props) => {
  const isMobile = window.innerWidth < 768

  const fighterId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { editedFighter } = globalState

  useEffect(() => {
    globalActions.fighters.getFighter(fighterId)
  }, [])

  const [pictureProfile, setPictureProfile] = useState()
  const [pictureCover, setPictureCover] = useState()

  const onDrop = async (picture) => {
    console.log('picture1', picture[0])
    setPictureProfile(picture[0])
    const formData = new FormData()
    formData.append('file', picture[0])
    try {
      const response = await axios.post('http://borntofight.live/api/files', formData)
      const url = response.data
      setPictureProfile(url)
      console.log('urlllll', url)
      globalActions.fighters.saveFighter({
          ...editedFighter, profilePicture: url}
      , fighterId)
        .then(() => {
          props.history.push('/')
        })
    } catch (error) {
      console.log('error')
    }
  }
  const onDrop2 = async (picture) => {
    console.log('picture2', picture[0])
    setPictureCover(picture[0])
    const formData = new FormData()
    formData.append('file', picture[0])
    try {
      const response = await axios.post('http://borntofight.live/api/files', formData)
      const url = response.data
      setPictureCover(url)
      globalActions.fighters.saveFighter({
          ...editedFighter, wallPicture: url}
        , fighterId)
        .then(() => {
          props.history.push('/')
        })
    } catch (error) {
      console.log('error')
    }
  }


  return (
    <React.Fragment>
      <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
        <Grid>
          <Grid.Column mobile={16} tablet={16} computer={16} textAlign='center'>
            <StyledCircularImage circular centered src={(editedFighter.profilePicture && !editedFighter.profilePicture.includes('static')) ? 'http://' + editedFighter.profilePicture : roundedPicture}/>
          </Grid.Column>
          <br/>

          <Grid.Column mobile={16} tablet={16} computer={16} textAlign='center'>
            <Flag name='ro'/>
            <span style={{fontWeight: 'bold', fontSize: '18px'}}>{editedFighter.country !== undefined && editedFighter.country.toUpperCase()}</span>
          </Grid.Column>
          <br/>

          <Grid.Column mobile={16} tablet={16} computer={16} textAlign='center'>
            <span>{editedFighter.firstName} &nbsp;</span>
            <span style={{fontWeight: 'bold'}}>{editedFighter.lastName}</span>

            <br/>
            <span>{editedFighter.gloryRecord}</span> <br/>
            {/*<span style={{fontWeight: 'bold'}}>(35 KO)</span>*/}
          </Grid.Column>
          <ImageUploader
            withIcon={false}
            buttonText={<Trans>pickImage</Trans>}
            onChange={onDrop}
            imgExtension={['.jpg', '.gif', '.png', '.gif']}
            maxFileSize={5242880}
          />
        </Grid>

      </Segment>
      <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
        <Image fluid src={(editedFighter.wallPicture && !editedFighter.wallPicture.includes('static')) ? 'http://' + editedFighter.wallPicture : coverPicture}/>
        <ImageUploader
          withIcon={false}
          buttonText={<Trans>pickImage</Trans>}
          onChange={onDrop2}
          imgExtension={['.jpg', '.gif', '.png', '.gif']}
          maxFileSize={5242880}
        />
      </Segment>
      <Segment style={{marginTop: '1rem', marginBottom: '1rem', textAlign: 'center'}}>
        <NavLink to={'/records/'+ fighterId}>
          <span style={{fontWeight: 'bold', color: 'black'}}><Trans>fightsHistory</Trans></span>
        </NavLink>
      </Segment>
    </React.Fragment>
  )
}
const StyledCircularImage = styled(Image)`
@media (max-width: 768px) {
    max-width: 70px;
    max-height: 70px;
    min-width: 70px;
    min-height: 70px;
    border: 3px solid black;
    margin: 0;
  }
  max-width: 150px;
  max-height: 150px;
  min-width: 150px;
  min-height: 150px;
  width: auto;
  height: auto;
  border: 5px solid black;
  margin: 0.5rem;
`

export default ProfilePictures


