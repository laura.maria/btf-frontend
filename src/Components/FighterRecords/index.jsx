import React, {useState, useEffect} from 'react'
import {Icon, Responsive, Table, Pagination, Image, Grid, Button, Item} from "semantic-ui-react";
import useGlobal from "../../Store";
import * as roundedPicture from '../../Assets/Images/circleImage.png'
import * as roundedPicture2 from '../../Assets/Images/profileImage.jpg'
import * as roundedPicture3 from '../../Assets/Images/prrofileImage.jpg'
import styled from "styled-components";
import {Link, NavLink} from "react-router-dom";
import {Trans} from "react-i18next";


const FighterRecords = (props) => {
  const isMobile = window.innerWidth < 768
  const fighterId = props.match.params.id || '0'
  const [globalState, globalActions] = useGlobal()
  const { editedFighter, fighterFights } = globalState

  useEffect(() => {
    globalActions.fighters.getFighter(fighterId)
  }, [])

  useEffect(() => {
    globalActions.fighters.getFighterFights(fighterId)
  }, [])


  return (
    <div style={{textAlign: 'center', width: '70%'}}>
      <div style={isMobile ? {paddingTop: '5%', fontSize: '18px'} : {paddingTop: '5%', fontSize: '36px'}}>
        <span><Trans>records</Trans></span>
      </div>
      { fighterFights !== undefined && fighterFights.length > 0 ? (
        <Table fluid striped style={{paddingTop: '5%', border: 'none'}}>
          <Responsive as={Table.Header} minWidth={768} style={{fontSize: '24px'}}>
            <Table.Row>
              <Table.HeaderCell>
                <Trans>match</Trans>
              </Table.HeaderCell>
              <Table.HeaderCell>
                <Trans>result</Trans>
              </Table.HeaderCell>
              <Table.HeaderCell>
                <Trans>view</Trans>
              </Table.HeaderCell>
            </Table.Row>
          </Responsive>

          <Table.Body>

            {fighterFights !== undefined && fighterFights.map((fight) => (
              <Table.Row textAlign='center' verticalAlign='middle'>
                <Table.Cell width={8}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>match</Trans>
                  </Responsive>
                  <Grid columns={2}>
                    <Grid.Column style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}} mobile={16}
                                 tablet={16} computer={7}>
                      <StyledCircularImage circular src={(editedFighter.profilePicture && !editedFighter.profilePicture.includes('static') ) ? 'http://' + editedFighter.profilePicture : roundedPicture}
                                           style={fight.win === 'WIN' ? {border: '3px solid green'} : (fight.win === 'LOSE' ? {border: '3px solid red'} : {border: '3px solid black'})}/>
                      <StyledCircularImage circular src={roundedPicture}
                                           style={fight.win === 'WIN' ? {border: '3px solid red'} : (fight.win === 'LOSE' ? {border: '3px solid green'} : {border: '3px solid black'})}/>
                    </Grid.Column>
                    <Grid.Column textAlign='center' verticalAlign='middle' style={{display: 'block'}} mobile={16}
                                 tablet={16}
                                 computer={9}>
                      <p>
                        <span style={{fontWeight: 'bold'}}>{editedFighter.lastName} &nbsp; </span>
                        {/*<span style={{color: 'grey', fontSize: '12px'}}>(25-0-0)</span>*/}
                        <span style={{fontWeight: 'bold'}}>vs {fight.opponent}</span>
                        {/*<span style={{color: 'grey', fontSize: '12px'}}>(30-5-1)</span>*/}
                      </p>
                      <p>
                        <span style={{fontWeight: 'bold'}}>{fight.fightDate}</span>
                        <span style={{color: 'grey', fontSize: '12px'}}>({fight.event})</span>
                      </p>
                    </Grid.Column>
                  </Grid>
                </Table.Cell>
                <Table.Cell width={6}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>result</Trans>
                  </Responsive>
                  <Grid columns={3}>
                    <Grid.Column style={{display: 'block'}}>
                      <p style={{color: 'grey'}}><Trans>round</Trans></p>
                      <p
                        style={{fontWeight: 'bold'}}>{fight.rounds === 'ONE' ? '1' : (fight.rounds === 'TWO' ? '2' : '3')}</p>
                    </Grid.Column>
                    <Grid.Column style={{display: 'block'}}>
                      <p style={{color: 'grey'}}><Trans>time</Trans></p>
                      <p style={{fontWeight: 'bold'}}>{fight.time}</p>
                    </Grid.Column>
                    <Grid.Column style={{display: 'block'}}>
                      <p style={{color: 'grey'}}><Trans>method</Trans></p>
                      <p
                        style={{fontWeight: 'bold'}}>{fight.method === 'KO/TKO' ? 'KO/TKO' : (fight.method === 'POINTS' ? 'Points' : 'Unanimity')}</p>
                    </Grid.Column>
                  </Grid>
                </Table.Cell>
                <Table.Cell width={2}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>view</Trans>
                  </Responsive>
                  <Item as={'a'} href={'/' + fight.videoLink} target="_blank"> <Icon name='youtube' color='red'
                                                                                     size='large'/></Item>
                </Table.Cell>
              </Table.Row>
            ))}

          </Table.Body>
        </Table>
      ) : (
        <p style={{paddingTop: '1rem'}}><Trans>noHistory</Trans></p>
      )}

    </div>
  );
}

const StyledCircularImage = styled(Image)`
  max-width: 80px;
  max-height: 80px;
  min-width: 80px;
  min-height: 80px;
  width: auto;
  height: auto;
  margin: 0.5rem;
`

export default FighterRecords


