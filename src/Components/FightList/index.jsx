import React, {useState, useEffect} from 'react'
import {
  Icon,
  Responsive,
  Table,
  Image,
  Grid,
  Button,
  Item,
  Header,
  Divider,
  Segment,
  Modal,
} from "semantic-ui-react";

import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import * as roundedPicture from "../../Assets/Images/circleImage.png";
import {Trans} from "react-i18next";

const FightList = (props) => {
  const isMobile = window.innerWidth < 768
  const fighterId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { fighterFights, editedFighter } = globalState
  const [modalOpen, setModalOpen] = useState(false)

  useEffect(() => {
    globalActions.fighters.getFighter(fighterId)
  }, [])
  useEffect(() => {
    globalActions.fighters.getFighterFights(fighterId)
  }, [])

  const deleteFight = (fightId) => {
    setModalOpen(false)
    globalActions.fighters.deleteFight(fightId)
      .then(() => {
      props.history.push('/')
    })
  }

  return (
    <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
      <Header style={{marginTop: '1rem'}}> Istoricul Luptelor </Header>
      <Divider/>
      <Table fluid striped style={{border: 'none'}}>
        <Responsive as={Table.Header} minWidth={768} style={{fontSize: '12px'}}>
          <Table.Row>
            <Table.HeaderCell>
              <Trans>match</Trans>
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Trans>result</Trans>
            </Table.HeaderCell>
            <Table.HeaderCell>
              Video
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Trans>edit</Trans>
            </Table.HeaderCell>
            <Table.HeaderCell>
              <Trans>delete</Trans>
            </Table.HeaderCell>
          </Table.Row>
        </Responsive>

        <Table.Body>
          {
            fighterFights !== undefined && fighterFights.map((fight) => (
              <Table.Row textAlign='center' verticalAlign='middle'>
                <Table.Cell width={9}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>Match</Trans>
                  </Responsive>
                  <Grid columns={2}>
                    <Grid.Column style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}} mobile={16}
                                 tablet={16} computer={7}>
                      <StyledCircularImage circular src={(editedFighter.profilePicture && !editedFighter.profilePicture.includes('static') ) ? 'http://' + editedFighter.profilePicture : roundedPicture}
                                           style={fight.win === 'WIN' ? {border: '3px solid green'} : (fight.win === 'LOSE' ? {border: '3px solid red'} : {border: '3px solid black'})}/>
                      <StyledCircularImage circular src={roundedPicture}
                                           style={fight.win === 'WIN' ? {border: '3px solid red'} : (fight.win === 'LOSE' ? {border: '3px solid green'} : {border: '3px solid black'})}/>
                    </Grid.Column>
                    <Grid.Column textAlign='center' verticalAlign='middle' style={{display: 'block'}} mobile={16}
                                 tablet={16}
                                 computer={9}>
                      <p>
                        <span style={{fontWeight: 'bold'}}>{editedFighter.lastName} &nbsp;</span>
                        {/*<span style={{color: 'grey', fontSize: '12px'}}>(25-0-0)</span>*/}
                        <span style={{fontWeight: 'bold'}}>vs {fight.opponent}</span>
                        {/*<span style={{color: 'grey', fontSize: '12px'}}>(31-6-0)</span>*/}
                      </p>
                      <p>
                        <span style={{fontWeight: 'bold'}}> {fight.fightDate}</span>
                        <span style={{color: 'grey', fontSize: '12px'}}>({fight.event})</span>
                      </p>
                    </Grid.Column>
                  </Grid>
                </Table.Cell>
                <Table.Cell width={5}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>result</Trans>
                  </Responsive>
                  <Grid columns={3}>
                    <Grid.Column style={{display: 'block'}} mobile={16} tablet={8} computer={5}>
                      <p style={{color: 'grey'}}><Trans>round</Trans></p>
                      <p
                        style={{fontWeight: 'bold'}}>{fight.rounds === 'ONE' ? '1' : (fight.rounds === 'TWO' ? '2' : '3')}</p>
                    </Grid.Column>
                    <Grid.Column style={{display: 'block'}} mobile={16} tablet={8} computer={5}>
                      <p style={{color: 'grey'}}><Trans>time</Trans></p>
                      <p style={{fontWeight: 'bold'}}>{fight.time}</p>
                    </Grid.Column>
                    <Grid.Column style={{display: 'block'}} mobile={16} tablet={16} computer={6}>
                      <p style={{color: 'grey'}}><Trans>method</Trans></p>
                      <p
                        style={{fontWeight: 'bold'}}>{fight.method === 'KO/TKO' ? 'KO/TKO' : (fight.method === 'POINTS' ? 'Points' : 'Unanimity')}</p>
                    </Grid.Column>
                  </Grid>
                </Table.Cell>
                <Table.Cell width={1}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>view</Trans>:
                  </Responsive>
                  <Item as={'a'} href={'/'+ fight.videoLink} target="_blank"> <Icon name='youtube' color='red' size='large'/></Item>
                </Table.Cell>

                <Table.Cell width={1}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>edit</Trans>:
                  </Responsive>
                  <Button onClick={ () => {props.history.push('/records/'+ fighterId + '/' + fight.id)}}> <Icon name='edit' color='black' size='small'/></Button>
                </Table.Cell>

                <Table.Cell width={1}>
                  <Responsive as='strong' maxWidth={767} style={{marginRight: 5}}>
                    <Trans>delete</Trans>:
                  </Responsive>
                  <Button onClick={ () => setModalOpen(true)}> <Icon name='delete' color='black' size='small'/></Button>
                </Table.Cell>

                <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
                  <Modal.Header>Sterge lupta</Modal.Header>
                  <Modal.Content>
                    <p>Sunteti sigur ca vreti sa stergeti aceasta lupta?</p>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button negative onClick={() => setModalOpen(false)}>Renunta</Button>
                    <Button color='green' onClick={() => deleteFight(fight.id)}>Sterge</Button>
                  </Modal.Actions>
                </Modal>

              </Table.Row>
            ))
          }
        </Table.Body>
      </Table>

    </Segment>
  )
}


const StyledCircularImage = styled(Image)`
  max-width: 60px;
  max-height: 60px;
  min-width: 60px;
  min-height: 60px;
  width: auto;
  height: auto;
  margin: 0.5rem;
`


export default FightList


