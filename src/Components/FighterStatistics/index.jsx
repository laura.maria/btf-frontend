import React, { useState, useEffect } from 'react'
import {Icon, Responsive, Table} from "semantic-ui-react";
import styled from "styled-components";
import useGlobal from "../../Store";
import {Trans} from "react-i18next";


const FighterStatistics = (props) => {
  const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  const [width, setWidth] = useState(window.innerWidth)

  const fighterId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { editedFighter } = globalState

  useEffect(() => {
    globalActions.fighters.getFighter(fighterId)
  }, [])

  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
  }, [])

  const updateDimensions = () => {
    setWidth(window.innerWidth)
    setIsMobile(width < 768)
  }
  return (
    <div style={width < 1180 ? {textAlign: 'center', width: '70%'} : {position: 'absolute', textAlign: 'center', width: '70%'}}>
      <div style={isMobile ? {paddingTop:'5%', fontSize: '18px'} : {paddingTop: '5%', fontSize: '36px'}}>
        <span><Trans>stats</Trans></span>&nbsp;<span
        style={{fontWeight: 'bold'}}><Trans>ofFighter</Trans></span>
      </div>
      <Table basic='very' fluid style={{color: 'white', paddingTop: '5%'}}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={6}></Table.HeaderCell>
            <Table.HeaderCell width={4}></Table.HeaderCell>
            <Table.HeaderCell width={6}></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <StyledWhiteCell><Icon name='trophy' color='red' style={{marginRight:'1rem'}}/>Glory Record</StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.gloryRecord}</StyledWhiteCell>
            <StyledWhiteCell><Trans>gloryRecord</Trans></StyledWhiteCell>
          </Table.Row>
          <Table.Row>
            <StyledWhiteCell><Icon name='stopwatch' color='red' style={{marginRight:'1rem'}}/>Timp mediu de lupta</StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.fightTime}</StyledWhiteCell>
            <StyledWhiteCell><Trans>fightDuration</Trans></StyledWhiteCell>
          </Table.Row>
          <Table.Row>
            <StyledWhiteCell><Icon name='chart bar' color='red' style={{marginRight:'1rem'}}/>Raport de knockdown</StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.knockdown}</StyledWhiteCell>
            <StyledWhiteCell>Knockdowns date / knockouts date</StyledWhiteCell>
          </Table.Row>
          <Table.Row>
            <StyledWhiteCell><Icon name='hourglass' color='red' style={{marginRight:'1rem'}}/>SLpM</StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.slpm}</StyledWhiteCell>
            <StyledWhiteCell>Stikes Landed per Minute</StyledWhiteCell>
          </Table.Row>
          <Table.Row>
            <StyledWhiteCell><Icon name='clock outline' color='red' style={{marginRight:'1rem'}}/>SApM</StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.sapm}</StyledWhiteCell>
            <StyledWhiteCell>Strikes Absorbed per Minute</StyledWhiteCell>
          </Table.Row>
          <Table.Row>
            <StyledWhiteCell><Icon name='balance scale' color='red' style={{marginRight:'1rem'}}/><Trans>punchDifference</Trans></StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.difference}</StyledWhiteCell>
            <StyledWhiteCell>Difference between SLpM and SApM</StyledWhiteCell>
          </Table.Row>
          <Table.Row>
            <StyledWhiteCell><Icon name='target' color='red' style={{marginRight:'1rem'}}/><Trans>precisionz</Trans></StyledWhiteCell>
            <StyledWhiteCell>{editedFighter.precisionz}</StyledWhiteCell>
            <StyledWhiteCell>In proportie cu loviturile reusite</StyledWhiteCell>
          </Table.Row>
        </Table.Body>
      </Table>
    </div>
  )
}
const StyledWhiteCell = styled(Table.Cell)`
  border-bottom: 1px solid ${props => typeof props.color !== 'undefined' ? props.color : 'rgba(255, 255, 255, .5)'} !important;
`

export default FighterStatistics
