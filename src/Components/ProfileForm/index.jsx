import React, {useState, useEffect} from 'react'
import {
  Icon,
  Responsive,
  Table,
  Pagination,
  Image,
  Grid,
  Button,
  Item,
  Header,
  Flag,
  Divider,
  Segment, Form
} from "semantic-ui-react";
import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import {Link, NavLink} from "react-router-dom";
import {Trans} from "react-i18next";


const ProfileForm = (props) => {
  const isMobile = window.innerWidth < 768

  const fighterId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { editedFighter } = globalState

  const [fighter, setFighter] = useState(editedFighter)


  useEffect(() => {
    if(fighterId !== '0') globalActions.fighters.getFighter(fighterId)
  }, [])

  const updateFighterState = e => {
    const { name, value } = typeof e.target !== 'undefined' ? e.target : e
    console.log(name, value)
    setFighter({ ...fighter, [name]: value })
  }

  const saveFighter = () => {
      globalActions.fighters.saveFighter({
          ...fighter}
       , fighterId)
        .then(() => {
          props.history.push('/')
        })

  }

  return (
    <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
      <Header style={{marginTop: '1rem'}}> Editeaza profil </Header>
      <Divider/>
      <Form>
        <Grid columns={2}>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='firstName' value={fighter.firstName || ''} fluid label={<Trans>lastName</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='lastName' value={fighter.lastName || ''} fluid label={<Trans>firstName</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='phone' value={fighter.phone || ''} fluid label={<Trans>phoneNumber</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='email' value={fighter.email || ''} fluid label={<Trans>email</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='club' value={fighter.club || ''} fluid label={<Trans>club</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='couch' value={fighter.couch || ''} fluid label={<Trans>couch</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='country' value={fighter.country || ''} fluid label={<Trans>country</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='nickname' value={fighter.nickname || ''} fluid label={<Trans>nickname</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={5} computer={5}>
            <Form.Input name='height' value={fighter.height || ''} fluid label={<Trans>height</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={5} computer={5}>
            <Form.Input name='weight' value={fighter.weight || ''} fluid label={<Trans>weight</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={6} computer={6}>
            <Form.Input name='age' value={fighter.age || ''} fluid label={<Trans>age</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Input name='category' value={fighter.category || ''} fluid label={<Trans>category</Trans>} onChange={updateFighterState}/>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Input name='place' value={fighter.place || ''} fluid label={<Trans>categoryPlace</Trans>} onChange={updateFighterState}/>
          </Grid.Column>
        </Grid>

        <Divider/>
        <Grid columns={2}>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='facebook' value={fighter.facebook || ''} fluid label='Facebook' onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='instagram' value={fighter.instagram || ''} fluid label='Instagram' onChange={updateFighterState}/>
          </StyledGridColumn>

          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Input name='youtube' value={fighter.youtube || ''} fluid label='Youtube' onChange={updateFighterState}/>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Input name='website' value={fighter.website || ''} fluid label={<Trans>personalSite</Trans>} onChange={updateFighterState}/>
          </Grid.Column>
        </Grid>
        <Divider/>

        <Grid>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='gloryRecord' value={fighter.gloryRecord || ''} fluid label={<Trans>gloryRecord</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='fightTime' value={fighter.fightTime || ''} fluid label={<Trans>fightTime</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='knockdown' value={fighter.knockdown || ''} fluid label={<Trans>knockdownReport</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='precisionz' value={fighter.precisionz || ''} fluid label={<Trans>precisionz</Trans>} onChange={updateFighterState}/>
          </StyledGridColumn>

          <Grid.Column mobile={16} tablet={5} fighter={5}>
            <Form.Input name='slpm' value={fighter.slpm || ''} fluid label='SLpM' onChange={updateFighterState}/>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={5} computer={5}>
            <Form.Input name='sapm' value={fighter.sapm || ''} fluid label='SApM' onChange={updateFighterState}/>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={6} computer={6}>
            <Form.Input name='difference' value={fighter.difference || ''} fluid label={<Trans>punchDifference</Trans>} onChange={updateFighterState}/>
          </Grid.Column>

        </Grid>
        <Divider/>
        <Grid columns={1}>
          <Grid.Column>
            <Form.TextArea name='description' label={<Trans>aboutFighter</Trans>} placeholder='...' value={fighter.description || ''} onChange={updateFighterState}/>
          </Grid.Column>
        </Grid>
        <div style={{textAlign: 'right', marginTop: '1rem'}}>
          <Button onClick={saveFighter}><Trans>save</Trans> &nbsp; <Trans>profile</Trans></Button>
        </div>
      </Form>

    </Segment>
  )
}


const StyledGridColumn = styled(Grid.Column)`
  padding-bottom: ${props => typeof props.paddingBottom !== 'undefined' ? props.paddingBottom : '0'} !important;
`
export default ProfileForm


