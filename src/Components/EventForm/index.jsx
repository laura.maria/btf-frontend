import React, {useState, useEffect} from 'react'
import {
  Grid,
  Button,
  Header,
  Divider,
  Segment, Form
} from "semantic-ui-react";
import {KeyboardDatePicker, KeyboardTimePicker} from '@material-ui/pickers'
import moment from 'moment'
import axios from 'axios'

import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import {Link, NavLink} from "react-router-dom";
import ImageUploader from 'react-images-upload';
import {Trans} from "react-i18next";

const EventForm = (props) => {
  const isMobile = window.innerWidth < 768
  const [globalState, globalActions] = useGlobal()
  const eventId = props.match.params.id || '0'

  const [event, setEvent] = useState({})


  const updateEventState = e => {
    const { name, value } = typeof e.target !== 'undefined' ? e.target : e
    setEvent({ ...event, [name]: value })
  }
  const onDateChange = (date) => {
    console.log('format', moment(date).format('YYYY-MM-DD'))
    setEvent({ ...event, date })
  }

  const onTimeChange = (time) => {
    console.log('format', moment(time).format('HH:mm:00'))
    setEvent({ ...event, time })
  }

  const saveEvent = async () => {

    globalActions.events.saveEvent({
        ...event,
      date: moment(event.date).format('YYYY-MM-DD'),
      time: moment(event.time).format('HH:mm:00'),
      eventPicture: picture || null}
      , eventId)
      .then(() => {
        props.history.push('/')
      })
  }

  const [picture, setPicture] = useState()

  const onDrop = async (picture) => {
    console.log('picture', picture[0])
    setPicture(picture[0])
    const formData = new FormData()
    formData.append('file', picture[0])
    try {
      const response = await axios.post('http://borntofight.live/api/files',  formData)
      const url = response.data
      setPicture(url)
    } catch (error) {
     console.log('error')
    }
  }

  return (
    <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
      <Header style={{marginTop: '1rem'}}> {eventId !== '0' ? (<Trans>edit</Trans>) : <Trans>add</Trans>} <Trans>event</Trans> </Header>
      <Divider/>
      <Form>
        <Grid columns={2}>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='organizationName' value={event.organizationName || ''} fluid label={<Trans>eventName</Trans>} onChange={updateEventState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='city' value={event.city || ''} fluid label={<Trans>city</Trans>} onChange={updateEventState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Field name='date' fluid>
              <label><Trans>eventDate</Trans></label>
              <KeyboardDatePicker
                value={event.date || new Date()}
                onChange={date => onDateChange(date)}
                animateYearScrolling
                format='DD.MM.YY'
                InputProps={{
                  disableUnderline: true,
                }}
                style={{width: '100%'}}/>
            </Form.Field>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='address' value={event.address || ''} fluid label={<Trans>locationAddress</Trans>} onChange={updateEventState}/>
          </StyledGridColumn>


          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Field name='time' fluid>
              <label><Trans>eventTime</Trans></label>
              <KeyboardTimePicker
                value={event.time || new Date()}
                onChange={time => onTimeChange(time)}
                ampm={false}
                InputProps={{
                  disableUnderline: true,
                }}
                style={{width: '100%'}}/>
            </Form.Field>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Input name='tickets' value={event.tickets || ''} fluid label={<Trans>tickets</Trans>} onChange={updateEventState}/>
          </Grid.Column>

          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.TextArea name='description' label={<Trans>description</Trans>} placeholder='...' value={event.description || ''} onChange={updateEventState}/>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <ImageUploader
              withIcon={true}
              buttonText={<Trans>pickImage</Trans>}
              onChange={onDrop}
              imgExtension={['.jpg', '.gif', '.png', '.gif']}
              maxFileSize={5242880}
            />
          </Grid.Column>

        </Grid>
        <div style={{textAlign: 'right', marginTop: '1rem'}}>
          <Button onClick={saveEvent}><Trans>save</Trans>&nbsp;<Trans>event</Trans></Button>
        </div>
      </Form>

    </Segment>
  )
}


const StyledGridColumn = styled(Grid.Column)`
  padding-bottom: ${props => typeof props.paddingBottom !== 'undefined' ? props.paddingBottom : '0'} !important;
`
export default EventForm


