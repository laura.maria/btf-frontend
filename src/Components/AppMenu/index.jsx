import React, {useEffect, useState} from 'react'
import {
  Container,
  Icon,
  Menu,
  Responsive,
  Sidebar,
  Flag
} from 'semantic-ui-react'
import {NavLink} from 'react-router-dom'
import logoMobile from '../../Assets/Images/logo-mobile.png'
import logo from '../../Assets/Images/logo.png'
import { withCookies } from 'react-cookie'
import {Trans} from "react-i18next";
import useGlobal from "../../Store";


const NavBarMobile = (props) => {
  const {children} = props
  const [visible, setVisible] = useState(false)

  const handleSidebarShow = () => setVisible(true)
  const handleSidebarHide = () => setVisible(false)

  const changeLanguage = (e,lng) => {
    const { cookies } = props
    cookies.set('language', lng, { path: '/' })

    window.location.reload()
  }
  const [globalState, globalActions] = useGlobal()
  const { myId } = globalState

  useEffect(() => {
    globalActions.fighters.getMyId()
  }, [])

  const logOut = () => {
    const { cookies } = props
    console.log('logout')
    //@todo delete cookies ????????????? document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
    cookies.removeAll()
  }


  return (
    <Sidebar.Pushable as='div'>
      <Sidebar as={Menu} animation='overlay' onHide={handleSidebarHide} icon='labeled' vertical visible={visible}
               color='#000000'
               style={{textAlign: 'left'}}>
        <Menu.Item>
          <img alt='logo' src={logoMobile} style={{width: 140, height: 50}}/>
        </Menu.Item>

        { myId !== undefined && (<Menu.Item as='div' key='my-profile' onClick={handleSidebarHide} style={{textAlign: 'left'}}>
          <NavLink to={'/my-profile/' + myId}>
            <Trans>myProfile</Trans>
          </NavLink>
        </Menu.Item>)}

        <Menu.Item as='div' key='events' onClick={handleSidebarHide} style={{textAlign: 'left'}}>
          <NavLink to='/events'>
            <Trans>events</Trans>
          </NavLink>
        </Menu.Item>

        <Menu.Item as='div' key='fighters' onClick={handleSidebarHide} style={{textAlign: 'left'}}>
          <NavLink to='/fighters'>
            <Trans>fighters</Trans>
          </NavLink>
        </Menu.Item>

        <Menu.Item as='div' key='contact' onClick={handleSidebarHide} style={{textAlign: 'left'}}>
          <NavLink to='/contact'>
            <Trans>contact</Trans>
          </NavLink>
        </Menu.Item>

        <Menu.Item as='a' key={'roFlag'}
                   onClick={(e) => {
                     changeLanguage(e, 'ro')
                   }}
                   style={{}}>
          <Flag name='ro'/>
        </Menu.Item>

        <Menu.Item as='a' key={'enFlag'}
                   onClick={(e) => changeLanguage(e,'en')}
                   style={{}}>
          <Flag name='gb'/>
        </Menu.Item>

        <Menu.Item as='a' key={'logOut'}
                   onClick={() => {
                     logOut()
                   }}
                   style={{fontSize: 24, padding: 7}}>
          <Icon name={'log out'}/>
        </Menu.Item>

      </Sidebar>

      <Sidebar.Pusher dimmed={visible} style={{minHeight: '100vh'}}>
        <Menu fixed='top'>
          <Menu.Item onClick={handleSidebarShow}>
            <Icon name='sidebar'/>
          </Menu.Item>
        </Menu>
        {children}
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  )
}

const NavBarDesktop = (props) => {
  const {history} = props
  const [activeIndex, setActiveIndex] = useState('')

  const navigateToPage = (page, key) => {
    setActiveIndex(key)
    history.push(page)
  }
  const changeLanguage = (e,lng) => {
    // e.preventDefault()
    const { cookies } = props
    cookies.set('language', lng, { path: '/' })

    window.location.reload()
  }

  const logOut = () => {
    // const { cookies } = props
    console.log('propsBefore', props)
    console.log('logout')
    //@todo delete cookies ???????????/  document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;"
    // cookies.remove()
      let cookies = document.cookie.split(";");

      for (let i = 0; i < cookies.length; i++) {
        const cookie = cookies[i];
        const eqPos = cookie.indexOf("=");
        const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
      }

    console.log('propsAfter', props)

  }

  const [globalState, globalActions] = useGlobal()
  const { myId } = globalState

  useEffect(() => {
    globalActions.fighters.getMyId()
  }, [])

  return (
    <Menu fixed='top' style={{backgroundColor: '#000000', paddingBottom: '1rem', paddingTop: '1rem', paddingLeft: '20%', paddingRight: '20%'}} inverted>
      <img alt='logo' src={logo} style={{width: 140, height: 50, marginRight: '2rem'}}/>

      <React.Fragment>
        <Menu.Menu position='left'>
          <Menu.Item as='a' active={activeIndex === 'events'} key={'events'}
                     onClick={() => navigateToPage('/events', 'events')}
                     style={{fontSize: 16, padding: 10}}>
            <Trans>events</Trans>
          </Menu.Item>

          <Menu.Item as='a' active={activeIndex === 'fighters'} key={'fighters'}
                     onClick={() => navigateToPage('/fighters', 'fighters')}
                     style={{fontSize: 16, padding: 10}}>
            <Trans>fighters</Trans>
          </Menu.Item>
        </Menu.Menu>

        <Menu.Menu position='right'>
          {myId !== undefined && (<Menu.Item as='a' active={activeIndex === 'my-profile'} key={'my-profile'}
                     onClick={() => myId !== undefined &&  navigateToPage('/my-profile/' + myId , 'my-profile')}
                     style={{fontSize: 16, padding: 10}}>
            <Icon name={'user'}/>
          </Menu.Item>)}
          <Menu.Item as='a' active={activeIndex === 'contact'} key={'contact'}
                     onClick={() => navigateToPage('/contact', 'contact')}
                     style={{fontSize: 16, padding: 10}}>
            <Trans>contact</Trans>
          </Menu.Item>
          <Menu.Item as='a' active={activeIndex === 'instagram'} key={'instagram'}
                     onClick={() => navigateToPage('/events', 'instagram')}
                     style={{fontSize: 24, padding: 10}}>
            <Icon name={'instagram'}/>
          </Menu.Item>
          <Menu.Item as='a' active={activeIndex === 'facebook'} key={'facebook'}
                     onClick={() => navigateToPage('/events', 'facebook')}
                     style={{fontSize: 24, padding: 7}}>
            <Icon name={'facebook'}/>
          </Menu.Item>

          <Menu.Item as='a' active={activeIndex === 'roFlag'} key={'roFlag'}
                     onClick={(e) => {
                       changeLanguage(e, 'ro')
                     }}
                     style={{}}>
            <Flag name='ro'/>
          </Menu.Item>

          <Menu.Item as='a' active={activeIndex === 'enFlag'} key={'enFlag'}
                     onClick={(e) => changeLanguage(e,'en')}
                     style={{}}>
            <Flag name='gb'/>
          </Menu.Item>

          <Menu.Item as='a' active={activeIndex === 'logOut'} key={'logOut'}
                     onClick={() => {
                       logOut()
                       navigateToPage('/events', 'logOut')
                     }}
                     style={{fontSize: 24, padding: 7}}>
            <Icon name={'log out'}/>
          </Menu.Item>

        </Menu.Menu>
      </React.Fragment>
    </Menu>
  )
}

const AppMenu = (props) => {
const {children, history} = props

  return (
    <React.Fragment>
      <Responsive {...Responsive.onlyMobile}>
        <NavBarMobile history={history} {...props}>
          <Container fluid style={{
            marginTop: '5.5rem',
            paddingBottom: '1rem'
          }}>{children}</Container>
        </NavBarMobile>
      </Responsive>
      <Responsive minWidth={Responsive.onlyTablet.minWidth}>
        <NavBarDesktop history={history} {...props}/>
        <Container fluid style={{
          marginTop: '5.5rem',
          paddingBottom: '1rem'
        }}>{children}</Container>
      </Responsive>
    </React.Fragment>
  )
}

export default withCookies(AppMenu)
