import React, {useState, useEffect} from 'react'
import {Icon, Responsive, Table, Pagination, Image, Grid, Button, Item, Header, Flag} from "semantic-ui-react";
import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import {Link, NavLink} from "react-router-dom";


const FighterDescription = (props) => {
  const isMobile = window.innerWidth < 768

  const fighterId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { editedFighter } = globalState

  useEffect(() => {
    globalActions.fighters.getFighter(fighterId)
  }, [])

  return (
    <div style={isMobile ? {textAlign: 'center'} : {textAlign: 'center', width: '70%'}}>
      <Grid columns={2} fluid verticalAlign='middle'>
        <Grid.Column mobile={16} tablet={6} computer={6}>
          <Image src={descriptionImage} style={{maxHeight: '80%'}}/>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={10} computer={10}>
          <Grid>
            <Grid.Row columns={2}>
              <Grid.Column width={2} textAlign='right'>
                <Flag name='ro'/>
              </Grid.Column>
              <Grid.Column width={14} textAlign='left'>
                <Header>{editedFighter.firstName} {editedFighter.lastName}</Header>
                <span>{editedFighter.gloryRecord}</span>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <p style={{paddingLeft: '5%', paddingRight: '5%', textAlign: 'left'}}>
                {editedFighter.description}
              </p>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      </Grid>
    </div>
  )
}

export default FighterDescription


