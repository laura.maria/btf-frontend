import React, {useState} from 'react'
import {Button, Form, Grid, Icon, Image, Item, List, Modal} from "semantic-ui-react";
import styled from "styled-components";
import * as logoImage from '../../Assets/Images/logoImage.png'
import {Trans} from "react-i18next";
import {NavLink} from "react-router-dom";


const Footer = (props) => {
  const isMobile = window.innerWidth < 768
  const mobileStyledGrid = {fontSize: '12px', backgroundColor: 'black', color: 'white'}
  const desktopStyledGrid = {
    paddingLeft: '15%',
    paddingRight: '15%',
    fontSize: '14px',
    backgroundColor: 'black',
    color: 'white'
  }
  const [modalOpen, setModalOpen] = useState(false)
  const [email, setEmail] = useState('')
  const subscribe = () => {
    console.log('subscribe', email)
    setModalOpen(false)

  }
  const updateEmail = e => {
    const { value } = typeof e.target !== 'undefined' ? e.target : e
    setEmail(value)
  }

  return (
    <Grid centered divided='vertically' style={isMobile ? mobileStyledGrid : desktopStyledGrid}>
      <Grid.Row columns={4} verticalAlign='middle' style={{borderBottom: '1px solid rgba(255,255,255,.5'}}>
        <Grid.Column mobile={16} tablet={10} computer={5}>
          <Image src={logoImage} fluid centered size='small'/>
        </Grid.Column>
        <Grid.Column mobile={8} tablet={3} computer={3} style={{paddingLeft: '5%'}}>
          <List divided relaxed style={{ display: 'grid' }}>
            <StyledListItem as={'a'} href={'/events'} target="_blank">
              > <Trans>events</Trans>
            </StyledListItem>
            <StyledListItem as={'a'} href={'/fighters'} target="_blank">
              > <Trans>fighters</Trans>
            </StyledListItem>
            <StyledListItem as={'a'} href={'/'} target="_blank">
              > Video
            </StyledListItem>
          </List>
        </Grid.Column>
        <Grid.Column mobile={8} tablet={3} computer={3} style={{paddingRight: '5%'}}>
          <List divided relaxed style={{ display: 'grid' }}>
            <StyledListItem as={'a'} href={'/'} target="_blank">
              > <Trans>news</Trans>
            </StyledListItem>
            <StyledListItem as={'a'} href={'/'} target="_blank">
              > <Trans>about</Trans>
            </StyledListItem>
            <StyledListItem as={'a'} href={'/'} target="_blank">
              > Calendar
            </StyledListItem>
          </List>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={16} computer={5}>
          <span style={isMobile ? {fontSize: '14px'} : {fontSize: '16px'}}>NEWSLETTER</span>
          <br/>
          <Button onClick={() => setModalOpen(true)}negative fluid circular style={{marginTop: '5%'}}> <Trans>subscribe</Trans> </Button>
        </Grid.Column>

        <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
          <Modal.Header>Abonare la newsletter</Modal.Header>
          <Modal.Content>
            <p>Care este adresa de email?</p>
            <Form>
              <Form.Input name='email' value={ email || ''} fluid label={<Trans>email</Trans>} onChange={updateEmail}/>
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button negative onClick={() => setModalOpen(false)}>Renunta</Button>
            <Button color='green' onClick={subscribe}>Abonare</Button>
          </Modal.Actions>
        </Modal>

      </Grid.Row>
      <Grid.Row columns={3}>
        <Grid.Column textAlign={'right'}>
          <span style={{fontSize: '11px'}}><Trans>cookiesAlert</Trans></span>
        </Grid.Column>
        <Grid.Column textAlign={'center'}>
          <Item as={'a'} href={'/'} target="_blank" style={{color: 'white'}}> <Icon name='youtube' size='large'/></Item>
          <Item as={'a'} href={'/'} target="_blank" style={{color: 'white'}}> <Icon name='twitter' size='large'/></Item>
          <Item as={'a'} href={'/'} target="_blank" style={{color: 'white'}}> <Icon name='instagram' size='large'/></Item>
          <Item as={'a'} href={'/'} target="_blank" style={{color: 'white'}}> <Icon name='facebook' size='large'/></Item>
        </Grid.Column>
        <Grid.Column textAlign={'center'}>
          <span style={{fontSize: '12px'}}>Copyright @borntofight 2020</span>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

const StyledListItem = styled(List.Item)`
  border-bottom: 1px solid ${props => typeof props.color !== 'undefined' ? props.color : 'rgba(255, 255, 255, .5)'} !important;
  color: white;
`

export default Footer
