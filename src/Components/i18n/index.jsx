import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { Cookies } from 'react-cookie'
import LanguageDetector from 'i18next-browser-languagedetector'
import English from './English.json'
import Romanian from './Romanian.json'

const cookies = new Cookies()
const language = cookies.get('language') || 'ro'

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      en: {
        translation: English
      },
      ro: {
        translation: Romanian
      }
    },
    lng: language,
    keySeparator: false,
    interpolation: {
      escapeValue: false
    }
  })

export default i18n
