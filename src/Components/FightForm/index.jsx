import React, {useState, useEffect} from 'react'
import {
  Grid,
  Button,
  Header,
  Divider,
  Segment, Form
} from "semantic-ui-react";
import {KeyboardDatePicker} from '@material-ui/pickers'

import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import {roundOptions, resultOptions, methodOptions} from '../../Config/selectOptions'
import {Trans} from "react-i18next";
import moment from 'moment'

const FightForm = (props) => {
  const isMobile = window.innerWidth < 768
  const fightId = props.match.params.extraId || '0'
  const fighterId = props.match.params.id || '0'
  const [globalState, globalActions] = useGlobal()
  const { editedFight } = globalState

  const [fight, setFight] = useState(editedFight)

  useEffect(() => {
    console.log('useEffect2')
    if(fightId !== '0') globalActions.fighters.getFight(fightId)
  }, [fightId])

  useEffect(() => {
    console.log('useEffect3', editedFight)
    setFight(editedFight)
  }, [editedFight])

  const updateFightState = e => {
    const { name, value } = typeof e.target !== 'undefined' ? e.target : e
    setFight({ ...fight, [name]: value })
  }

  const onDateChange = (date) => {
    console.log('format', moment(date).format('YYYY-MM-DD'))
    setFight({ ...fight, fightDate: date })
  }

  const saveFight = () => {
    console.log('apas save fight', fight)
    globalActions.fighters.saveFight({
        ...fight,
        fightDate: moment(fight.fightDate).format('YYYY-MM-DD')}
      , fighterId, fightId)
      .then(() => {
        props.history.push('/')
      })
      .catch(e => console.log(e))

  }

  return (
    <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
      <Header style={{marginTop: '1rem'}}> {fightId !== '0' ? <Trans>edit</Trans> : <Trans>add</Trans>} <Trans>fight</Trans> </Header>
      <Divider/>
      <Form>
        <Grid columns={2}>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='opponent' value={fight.opponent || ''} fluid label={<Trans>opponentName</Trans>} onChange={updateFightState}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Select name='win' value={fight.win || ''} fluid label={<Trans>result</Trans>} options={resultOptions} onChange={(e, data) => updateFightState(data)}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Field name='date' fluid>
              <label><Trans>eventDate</Trans></label>
              <KeyboardDatePicker
                value={fight.fightDate || new Date()}
                onChange={date => onDateChange(date)}
                animateYearScrolling
                format='MM/DD/YY'
                InputProps={{
                  disableUnderline: true,
                }}
                style={{width: '100%'}}/>
            </Form.Field>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='event' value={fight.event || ''} fluid label={<Trans>event</Trans>} onChange={updateFightState}/>
          </StyledGridColumn>

          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Select name='rounds' value={fight.rounds || ''} fluid label={<Trans>numberOfRounds</Trans>} options={roundOptions} onChange={(e, data) => updateFightState(data)}/>
          </StyledGridColumn>
          <StyledGridColumn mobile={16} tablet={8} computer={8}>
            <Form.Input name='time' value={fight.time || ''} fluid label={<Trans>totalTime</Trans>} onChange={updateFightState}/>
          </StyledGridColumn>

          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Select name='method' value={fight.method || ''} fluid label={<Trans>method</Trans>} options={methodOptions} onChange={(e, data) => updateFightState(data)}/>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <Form.Input name='videoLink' value={fight.videoLink || ''} fluid label={<Trans>link</Trans>} onChange={updateFightState}/>
          </Grid.Column>
        </Grid>
        <div style={{textAlign: 'right', marginTop: '1rem'}}>
          <Button onClick={saveFight}><Trans>save</Trans> <Trans>fight</Trans></Button>
        </div>
      </Form>

    </Segment>
  )
}


const StyledGridColumn = styled(Grid.Column)`
  padding-bottom: ${props => typeof props.paddingBottom !== 'undefined' ? props.paddingBottom : '0'} !important;
`
export default FightForm


