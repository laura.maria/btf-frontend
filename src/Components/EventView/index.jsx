import React, {useState, useEffect} from 'react'
import {
  Icon,
  Responsive,
  Table,
  Pagination,
  Image,
  Grid,
  Button,
  Item,
  Header,
  Flag,
  Divider,
  Segment, Form, List
} from "semantic-ui-react";
import * as eventPicture from '../../Assets/Images/georgeFlorescu.png'
import {KeyboardDatePicker, KeyboardTimePicker} from '@material-ui/pickers'
import InputAdornment from '@material-ui/core/InputAdornment'

import useGlobal from "../../Store";
import * as descriptionImage from '../../Assets/Images/descriptionImage.png'
import styled from "styled-components";
import {Link, NavLink} from "react-router-dom";
import {roundOptions, resultOptions, methodOptions} from '../../Config/selectOptions'
import ImageUploader from 'react-images-upload';
import {Trans} from "react-i18next";

const EventView = (props) => {
  const isMobile = window.innerWidth < 768
  const eventId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { editedEvent } = globalState

  useEffect(() => {
    globalActions.events.getEvent(eventId)
  }, [])

  return (
    <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
      <Grid>
        <Grid.Row style={{padding: 0}}>
          <Image src={(editedEvent.eventPicture && !editedEvent.eventPicture.includes('static')) ? 'http://' + editedEvent.eventPicture : eventPicture} fluid/>
        </Grid.Row>
        <Grid.Row columns={2} style={{paddingRight: '5%', paddingLeft: '5%'}}>
          <Grid.Column textAlign='left' verticalAlign='middle'>
            <List divided relaxed>
              <List.Item>
                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description><Trans>organisedBy</Trans></List.Description>
                  <List.Header>{editedEvent.organizationName}</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='calendar alternate outline' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description><Trans>date</Trans>:</List.Description>
                  <List.Header>{editedEvent.date}</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='clock outline' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description><Trans>hour</Trans>:</List.Description>
                  <List.Header>{editedEvent.time}</List.Header>
                </List.Content>
              </List.Item>
            </List>
          </Grid.Column>

          <Grid.Column textAlign='left' verticalAlign='middle'>
            <List divided relaxed>
              <List.Item>
                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description><Trans>location</Trans>:</List.Description>
                  <List.Header>{editedEvent.city}</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description> </List.Description>
                  <List.Header>{editedEvent.address}</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='ticket' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description><Trans>ticket</Trans>:</List.Description>
                  <List.Header>{editedEvent.tickets}</List.Header>
                </List.Content>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <p style={{paddingLeft: '5%', paddingRight: '5%'}}>
            {editedEvent.description}
          </p>
        </Grid.Row>
      </Grid>

    </Segment>
  )
}

export default EventView


