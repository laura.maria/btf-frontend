import React, {useEffect, useState} from 'react'
import {Button, Divider, Form, Grid, Header, Segment, Container, Item, Icon, Modal} from "semantic-ui-react";
import styled from "styled-components";
import Footer from "../../Components/Footer";
import {Trans} from "react-i18next";
import moment from "../../Components/EventForm";
import useGlobal from "../../Store";

const Contact = (props) => {
  const [contact, setContact] = useState({})
  const [, globalActions] = useGlobal()

  const updateContactState = e => {
    const { name, value } = typeof e.target !== 'undefined' ? e.target : e
    setContact({ ...contact, [name]: value })
  }

  const saveContact = () => {

    globalActions.contact.saveContact({...contact})
      .then(() => {
        props.history.push('/')
      })

  }
  return (
    <Container>
      <Segment style={{marginTop: '1rem', marginBottom: '1rem'}}>
        <Header style={{marginTop: '1rem'}}> <Trans>contactForm</Trans> </Header>
        <Divider/>
        <Form>
          <Grid columns={2}>
            <StyledGridColumn mobile={16} tablet={8} computer={8}>
              <Form.Input name='firstName' value={contact.firstName || ''} fluid label={<Trans>lastName</Trans>} onChange={updateContactState}/>
            </StyledGridColumn>
            <StyledGridColumn mobile={16} tablet={8} computer={8}>
              <Form.Input name='lastName' value={contact.lastName || ''} fluid label={<Trans>firstName</Trans>} onChange={updateContactState}/>
            </StyledGridColumn>

            <StyledGridColumn mobile={16} tablet={8} computer={8}>
              <Form.Input name='phone' value={contact.phone || ''} fluid label={<Trans>phoneNumber</Trans>} onChange={updateContactState}/>
            </StyledGridColumn>
            <StyledGridColumn mobile={16} tablet={8} computer={8}>
              <Form.Input name='email' value={contact.email || ''} fluid label={<Trans>email</Trans>} onChange={updateContactState}/>
            </StyledGridColumn>

            <StyledGridColumn mobile={16} tablet={16} computer={16}>
              <Form.Input name='subject' value={contact.subject || ''} fluid label={<Trans>subject</Trans>} onChange={updateContactState}/>
            </StyledGridColumn>

            <Grid.Column mobile={16} tablet={16} computer={16}>
              <Form.Input name='message' value={contact.message || ''} fluid label={<Trans>message</Trans>} onChange={updateContactState}/>
            </Grid.Column>


          </Grid>

          <Divider/>
          <Grid>
            <Grid.Column mobile={16} tablet={16} computer={16} textAlign={'center'}>
              <Item as={'a'} href={'/'} target="_blank" style={{color: 'black'}}> <Icon name='youtube'
                                                                                        size='large'/></Item>
              <Item as={'a'} href={'/'} target="_blank" style={{color: 'black'}}> <Icon name='twitter'
                                                                                        size='large'/></Item>
              <Item as={'a'} href={'/'} target="_blank" style={{color: 'black'}}> <Icon name='instagram' size='large'/></Item>
              <Item as={'a'} href={'/'} target="_blank" style={{color: 'black'}}> <Icon name='facebook'
                                                                                        size='large'/></Item>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={16} computer={16} textAlign={'center'}>
              <strong>Email: contact@borntofight.ro</strong>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={16} computer={16} textAlign={'center'}>
              <strong> <Trans>phoneNumber</Trans>: 0747333445 </strong>
            </Grid.Column>
          </Grid>

          <div style={{textAlign: 'right', marginTop: '1rem'}}>
            <Button onClick={saveContact}><Trans>sendMessage</Trans></Button>
          </div>
        </Form>

      </Segment>
    </Container>
  )
}
const StyledGridColumn = styled(Grid.Column)`
  padding-bottom: ${props => typeof props.paddingBottom !== 'undefined' ? props.paddingBottom : '0'} !important;
`
export default Contact
