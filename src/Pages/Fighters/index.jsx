import React, {useEffect, useState} from 'react'
import {Container, Flag, Grid, Image, List, Table, Search, Form} from "semantic-ui-react";
import * as roundedPicture from "../../Assets/Images/circleImage.png";
import * as roundedPicture2 from '../../Assets/Images/profileImage.jpg'
import * as roundedPicture3 from '../../Assets/Images/prrofileImage.jpg'
import styled from "styled-components";
import {NavLink} from "react-router-dom";
import useGlobal from "../../Store";
import * as wallPicture from "../../Assets/Images/georgeFlorescu.png";
import {Trans} from "react-i18next";

const Fighters = (props) => {
  const [globalState, globalActions] = useGlobal()
  const { fighters } = globalState
  const [filters, setFilters] = useState('')


  useEffect(() => {
    globalActions.fighters.getFighters()
  }, [])

  const updateFilters = e => {
    const { value } = typeof e.target !== 'undefined' ? e.target : e
    setFilters(value)
  }

  const searchFighters = async () => {
    if (filters !== '') globalActions.search.getFightersSearch(filters)
    else globalActions.fighters.getFighters()
  }
  return (
    <Container>
      {/*<StyledSearch*/}
      {/*  fluid*/}
      {/*  size={'large'}*/}
      {/*  // loading={isLoading}*/}
      {/*  // onResultSelect={this.handleResultSelect}*/}
      {/*  // onSearchChange={_.debounce(this.handleSearchChange, 500, {*/}
      {/*  //   leading: true,*/}
      {/*  // })}*/}
      {/*  // results={results}*/}
      {/*  // value={value}*/}
      {/*  // {...this.props}*/}
      {/*/>*/}

      <Form>
        <Form.Input name='filters' value={filters || ''} placeholder='Search' fluid onChange={updateFilters} onKeyPress={searchFighters} style={{paddingTop: '1rem'}}/>
      </Form>

      <Table striped>
        { fighters.map((fighter) => (
        <StyledTableRow onClick={()=>{props.history.push('/fighters/'+ fighter.id)}}>
          <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>
            <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>
              <StyledCircularImage circular centered src={(fighter.profilePicture && !fighter.profilePicture.includes('static') ) ? 'http://' + fighter.profilePicture : roundedPicture}/>
            </Grid.Column>
            <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>
              <Grid>
                <Grid.Row columns={3}>
                  <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={6} computer={6}>
                    <span>{fighter.firstName}</span> <br/>
                    <span style={{fontWeight: 'bold'}}>{fighter.lastName}</span>
                  </Grid.Column>
                  <Grid.Column mobile={16} tablet={5} computer={5} textAlign='center'>
                    <Flag name='ro'/>
                    <span style={{fontWeight: 'bold', fontSize: '14px', textAlign: 'left'}}>{fighter.country !== undefined && fighter.country.toUpperCase()}</span>
                  </Grid.Column>
                  <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={5} computer={5}>
                    <span>{fighter.gloryRecord}</span> <br/>
                    <span style={{fontWeight: 'bold'}}>(35 KO)</span>
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                  <Grid.Column textAlign='left' verticalAlign='middle'>
                    <List divided relaxed>
                      <List.Item>
                        <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>
                        <List.Content>
                          <List.Description><Trans>ranking</Trans>:</List.Description>
                          <List.Header>{fighter.place}</List.Header>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>
                        <List.Content>
                          <List.Description><Trans>category</Trans>:</List.Description>
                          <List.Header>{fighter.category}</List.Header>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon name='user' size='large' color='red' verticalAlign='middle'/>
                        <List.Content>
                          <List.Description><Trans>nickname</Trans>:</List.Description>
                          <List.Header>{fighter.nickname}</List.Header>
                        </List.Content>
                      </List.Item>
                    </List>
                  </Grid.Column>

                  <Grid.Column textAlign='left' verticalAlign='middle'>
                    <List divided relaxed>
                      <List.Item>
                        <List.Icon name='resize vertical' size='large' color='red' verticalAlign='middle'/>
                        <List.Content>
                          <List.Description><Trans>height</Trans>:</List.Description>
                          <List.Header>{fighter.height} cm</List.Header>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>
                        <List.Content>
                          <List.Description><Trans>weight</Trans>:</List.Description>
                          <List.Header>{fighter.weight} kg/ {Math.floor(fighter.weight * 2.20462262)} lbs</List.Header>
                        </List.Content>
                      </List.Item>
                      <List.Item>
                        <List.Icon name='birthday cake' size='large' color='red' verticalAlign='middle'/>
                        <List.Content>
                          <List.Description><Trans>age</Trans>:</List.Description>
                          <List.Header>{fighter.age}</List.Header>
                        </List.Content>
                      </List.Item>
                    </List>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Grid.Column>
          </Grid>
        </StyledTableRow>
        ))}
        {/*<StyledTableRow onClick={()=>{props.history.push('/fighters/1')}}>*/}
        {/*  <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>*/}
        {/*    <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>*/}
        {/*      <StyledCircularImage circular centered src={roundedPicture}/>*/}
        {/*    </Grid.Column>*/}
        {/*    <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>*/}
        {/*      <Grid>*/}
        {/*        <Grid.Row columns={3}>*/}
        {/*          <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={6} computer={6}>*/}
        {/*            <span>George</span> <br/>*/}
        {/*            <span style={{fontWeight: 'bold'}}>Florescu</span>*/}
        {/*          </Grid.Column>*/}
        {/*          <Grid.Column mobile={16} tablet={5} computer={5} textAlign='center'>*/}
        {/*            <Flag name='ro'/>*/}
        {/*            <span style={{fontWeight: 'bold', fontSize: '14px', textAlign: 'left'}}>ROMANIA</span>*/}
        {/*          </Grid.Column>*/}
        {/*          <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={5} computer={5}>*/}
        {/*            <span>61-11-1</span> <br/>*/}
        {/*            <span style={{fontWeight: 'bold'}}>(35 KO)</span>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*        <Grid.Row columns={2}>*/}
        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Clasament:</List.Description>*/}
        {/*                  <List.Header>Campion categoria usoara</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Categorie:</List.Description>*/}
        {/*                  <List.Header>Usoara</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='user' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Nickname:</List.Description>*/}
        {/*                  <List.Header>FG</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}

        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='resize vertical' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Inaltime:</List.Description>*/}
        {/*                  <List.Header>178 cm/ 5'10''</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Greutate:</List.Description>*/}
        {/*                  <List.Header>69.4 kg/ 153 lbs</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='birthday cake' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Varsta:</List.Description>*/}
        {/*                  <List.Header>22</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*      </Grid>*/}
        {/*    </Grid.Column>*/}
        {/*  </Grid>*/}
        {/*</StyledTableRow>*/}


        {/*<StyledTableRow onClick={()=>{props.history.push('/fighters/2')}}>*/}
        {/*  <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>*/}
        {/*    <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>*/}
        {/*      <StyledCircularImage circular centered src={roundedPicture2}/>*/}
        {/*    </Grid.Column>*/}
        {/*    <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>*/}
        {/*      <Grid>*/}
        {/*        <Grid.Row columns={3}>*/}
        {/*          <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={6} computer={6}>*/}
        {/*            <span>Ion</span> <br/>*/}
        {/*            <span style={{fontWeight: 'bold'}}>Popescu</span>*/}
        {/*          </Grid.Column>*/}
        {/*          <Grid.Column mobile={16} tablet={5} computer={5} textAlign='center'>*/}
        {/*            <Flag name='ro'/>*/}
        {/*            <span style={{fontWeight: 'bold', fontSize: '14px', textAlign: 'left'}}>ROMANIA</span>*/}
        {/*          </Grid.Column>*/}
        {/*          <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={5} computer={5}>*/}
        {/*            <span>12-5-1</span> <br/>*/}
        {/*            <span style={{fontWeight: 'bold'}}>(5 KO)</span>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*        <Grid.Row columns={2}>*/}
        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Clasament:</List.Description>*/}
        {/*                  <List.Header>Campion categoria usoara</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Categorie:</List.Description>*/}
        {/*                  <List.Header>Usoara</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='user' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Nickname:</List.Description>*/}
        {/*                  <List.Header>Pop</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}

        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='resize vertical' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Inaltime:</List.Description>*/}
        {/*                  <List.Header>175 cm/ 5'9''</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Greutate:</List.Description>*/}
        {/*                  <List.Header>70.4 kg/ 155 lbs</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='birthday cake' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Varsta:</List.Description>*/}
        {/*                  <List.Header>24</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*      </Grid>*/}
        {/*    </Grid.Column>*/}
        {/*  </Grid>*/}
        {/*</StyledTableRow>*/}


        {/*<StyledTableRow onClick={()=>{props.history.push('/fighters/3')}}>*/}
        {/*  <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>*/}
        {/*    <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>*/}
        {/*      <StyledCircularImage circular centered src={roundedPicture3}/>*/}
        {/*    </Grid.Column>*/}
        {/*    <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>*/}
        {/*      <Grid>*/}
        {/*        <Grid.Row columns={3}>*/}
        {/*          <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={6} computer={6}>*/}
        {/*            <span>Ivan</span> <br/>*/}
        {/*            <span style={{fontWeight: 'bold'}}>Antoniu</span>*/}
        {/*          </Grid.Column>*/}
        {/*          <Grid.Column mobile={16} tablet={5} computer={5} textAlign='center'>*/}
        {/*            <Flag name='ro'/>*/}
        {/*            <span style={{fontWeight: 'bold', fontSize: '14px', textAlign: 'left'}}>ROMANIA</span>*/}
        {/*          </Grid.Column>*/}
        {/*          <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={5} computer={5}>*/}
        {/*            <span>31-10-2</span> <br/>*/}
        {/*            <span style={{fontWeight: 'bold'}}>(15 KO)</span>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*        <Grid.Row columns={2}>*/}
        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Clasament:</List.Description>*/}
        {/*                  <List.Header>Campion categoria grea</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Categorie:</List.Description>*/}
        {/*                  <List.Header>Grea</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='user' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Nickname:</List.Description>*/}
        {/*                  <List.Header>Ivanny</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}

        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='resize vertical' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Inaltime:</List.Description>*/}
        {/*                  <List.Header>180 cm/ 5'11''</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Greutate:</List.Description>*/}
        {/*                  <List.Header>90.4 kg/ 199 lbs</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='birthday cake' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Varsta:</List.Description>*/}
        {/*                  <List.Header>26</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*      </Grid>*/}
        {/*    </Grid.Column>*/}
        {/*  </Grid>*/}
        {/*</StyledTableRow>*/}

      </Table>
    </Container>
  )
}

const StyledCircularImage = styled(Image)`
@media (max-width: 768px) {
    max-width: 70px;
    max-height: 70px;
    min-width: 70px;
    min-height: 70px;
    border: 3px solid black;
    margin: 0;
  }
  max-width: 150px;
  max-height: 150px;
  min-width: 150px;
  min-height: 150px;
  width: auto;
  height: auto;
  border: 5px solid black;
  margin: 0.5rem;
`

const StyledTableRow = styled(Table.Row)`
:hover {
background-color: gainsboro !important;
cursor: pointer;
}
`

const StyledSearch = styled(Search)`
padding-top: 1rem;
div:first-of-type {
width: 100%;
}
`
export default Fighters
