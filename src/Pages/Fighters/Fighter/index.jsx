import React, {useState, useEffect} from 'react'
import {Divider, Flag, Grid, Icon, Image, List, Responsive, Table} from "semantic-ui-react"
import * as firstPicture from '../../../Assets/Images/georgeFlorescu.png'
import * as roundedPicture from '../../../Assets/Images/circleImage.png'
import * as backImage from '../../../Assets/Images/blackFight.png'
import styled from "styled-components";
import FighterStatistics from "../../../Components/FighterStatistics";
import FighterRecords from "../../../Components/FighterRecords";
import FighterDescription from "../../../Components/FighterDescription";
import FightList from "../../../Components/FightList";
import useGlobal from "../../../Store";


const Fighter = (props) => {
  // const [isMobile, setIsMobile] = useState(window.innerWidth < 768)
  // const [isTablet, setIsTablet] = useState(window.innerWidth >=768 && window.innerWidth < 1024)
  // const [isDesktop, setIsDesktop] = useState(window.innerWidth >= 1024)

  const isMobile = window.innerWidth < 768
  const fighterId = props.match.params.id || '0'

  const [globalState, globalActions] = useGlobal()
  const { editedFighter } = globalState

  useEffect(() => {
    globalActions.fighters.getFighter(fighterId)
  }, [])

  const [width, setWidth] = useState(window.innerWidth)

  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
  }, [])

  const updateDimensions = () => {
    setWidth(window.innerWidth)
  }

  // style={width >= 1024 ? {paddingLeft: '15%', paddingRight: '15%', fontSize: '16px', top: '-11rem'} : ((width >= 768 && width < 1024) ? {paddingLeft: '15%', paddingRight: '15%', fontSize: '16px', top: '-8rem'} : {fontSize: '12px', top: '-8rem'})}>


  return (
    <React.Fragment>
      <Grid columns={3}>
        <Grid.Row style={{paddingBottom: 0}}>
          <Image src={(editedFighter.wallPicture && !editedFighter.wallPicture.includes('static')) ? 'http://' + editedFighter.wallPicture : firstPicture} fluid/>
        </Grid.Row>
        <OpacityGridRow centered style={isMobile ? {height: '5rem', fontSize: '14px', top: '-5rem'} : {
          paddingLeft: '15%',
          paddingRight: '15%',
          height: '6rem',
          fontSize: '36px',
          top: '-6rem'
        }}>
          <Grid.Column textAlign='centered' verticalAlign='middle'>
            <span>{editedFighter.firstName}</span> <br/>
            <span style={{fontWeight: 'bold'}}>{editedFighter.lastName}</span>
          </Grid.Column>
          <Grid.Column></Grid.Column>
          <Grid.Column textAlign='centered' verticalAlign='middle'>
            <span style={{fontSize: '28px'}}>{editedFighter.gloryRecord}</span> <br/>
            {/*<span style={{fontWeight: 'bold'}}>(35 KO)</span>*/}
          </Grid.Column>
        </OpacityGridRow>
        <Grid.Row centered
                  style={width >= 1024 ? {paddingLeft: '15%', paddingRight: '15%', fontSize: '16px', top: '-11rem'} : ((width >= 768 && width < 1024) ? {paddingLeft: '15%', paddingRight: '15%', fontSize: '16px', top: '-8rem'} : {fontSize: '12px', top: '-7rem'})}>
        <Grid.Column textAlign='left' verticalAlign='middle'>
            <List divided relaxed>
              <List.Item>
                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description>Clasament:</List.Description>
                  <List.Header>{editedFighter.place}</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description>Categorie:</List.Description>
                  <List.Header>{editedFighter.category}</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='user' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description>Nickname:</List.Description>
                  <List.Header>{editedFighter.nickname}</List.Header>
                </List.Content>
              </List.Item>
            </List>
          </Grid.Column>
          <Grid.Column textAlign='centered' verticalAlign='middle'>
            <>
              <StyledCircularImage circular centered src={(editedFighter.profilePicture && !editedFighter.profilePicture.includes('static'))? 'http://' + editedFighter.profilePicture : roundedPicture}/>
              <br/>
              <Flag name='ro'/>
              <span style={{fontWeight: 'bold', fontSize: '18px'}}>{editedFighter.country !== undefined && editedFighter.country.toUpperCase()}</span>
            </>
          </Grid.Column>
          <Grid.Column textAlign='left' verticalAlign='middle'>
            <List divided relaxed>
              <List.Item>
                <List.Icon name='resize vertical' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description>Inaltime:</List.Description>
                  <List.Header>{editedFighter.height} cm</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='weight' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description>Greutate:</List.Description>
                  <List.Header>{editedFighter.weight} kg/ {Math.floor(editedFighter.weight * 2.20462262)} lbs</List.Header>
                </List.Content>
              </List.Item>
              <List.Item>
                <List.Icon name='birthday cake' size='large' color='red' verticalAlign='middle'/>
                <List.Content>
                  <List.Description>Varsta:</List.Description>
                  <List.Header>{editedFighter.age}</List.Header>
                </List.Content>
              </List.Item>
            </List>
          </Grid.Column>
        </Grid.Row>
        {/*</Grid>*/}
        {/*<Grid>*/}
        <Grid.Row centered style={ width < 1024 ? {backgroundColor: 'black', color: 'white', top: '-8rem'} : {backgroundColor: 'black', color: 'white', top: '-11rem'}}>
          <Responsive minWidth={1180}>
            <Image src={backImage} style={{position: 'relative'}}/>
          </Responsive>
          <FighterStatistics {...props}/>
        </Grid.Row>
        <Grid.Row centered style={width < 1024 ? {top: '-8rem'} : { top: '-11rem'}}>
          <FighterRecords {...props}/>
          {/*<FightList/>*/}
        </Grid.Row>
        <Grid.Row centered style={width < 1024 ? {top: '-8rem'} : { top: '-11rem'}}>
          <FighterDescription {...props}/>
        </Grid.Row>
      </Grid>
    </React.Fragment>
  )
}

const OpacityGridRow = styled(Grid.Row)`
  background-color: ${props => typeof props.color !== 'undefined' ? props.color : 'rgb(128,128,128,0.2)'} !important;
  color: white
`

const StyledCircularImage = styled(Image)`
@media (max-width: 768px) {
    max-width: 120px;
    max-height: 120px;
    min-width: 120px;
    min-height: 120px;
    margin: 0;
  }
  max-width: 220px;
  max-height: 220px;
  min-width: 220px;
  min-height: 220px;
  width: auto;
  height: auto;
  margin: 0.5rem;
`

export default Fighter
