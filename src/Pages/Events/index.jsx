import React, {useEffect, useState} from 'react'
import {Container, Form, Grid, List, Table, Image, Search, Icon, Button, Item} from "semantic-ui-react";
import * as roundedPicture from "../../Assets/Images/circleImage.png";
import * as eventPicture from '../../Assets/Images/georgeFlorescu.png'
import * as eventPicture2 from '../../Assets/Images/eventShow.png'
import styled from "styled-components";
import {NavLink} from "react-router-dom";
import useGlobal from "../../Store";
import {Trans} from "react-i18next";
import moment from "../../Components/EventForm";

const Events = (props) => {

  const [globalState, globalActions] = useGlobal()
  const { events } = globalState
  const [filters, setFilters] = useState('')

  useEffect(() => {
    globalActions.events.getEvents()
  }, [])

  const updateFilters = e => {
    const { value } = typeof e.target !== 'undefined' ? e.target : e
    setFilters(value)
  }

  const searchEvents = async () => {
    if (filters !== '') globalActions.search.getEventsSearch(filters)
    else globalActions.events.getEvents()
  }

  return (
    <Container>
      <Grid>
        <Grid.Column mobile={14} tablet={14} computer={14}>
          {/*<StyledSearch*/}
          {/*  fluid*/}
          {/*  size={'large'}*/}
          {/*  // loading={isLoading}*/}
          {/*  // onResultSelect={this.handleResultSelect}*/}
          {/*  // onSearchChange={_.debounce(this.handleSearchChange, 500, {*/}
          {/*  //   leading: true,*/}
          {/*  // })}*/}
          {/*  // results={results}*/}
          {/*  // value={value}*/}
          {/*  // {...this.props}*/}
          {/*/>*/}
          <Form>
            <Form.Input name='filters' value={filters || ''} placeholder='Search' fluid onChange={updateFilters} onKeyPress={searchEvents} style={{paddingTop: '1rem'}}/>
          </Form>
        </Grid.Column>
        <Grid.Column mobile={2} tablet={2} computer={2} style={{marginTop: '1.1rem'}}>
          <Button onClick={() => props.history.push('/event/0')}>
            <Icon name={'add'}/>
          </Button>
        </Grid.Column>
      </Grid>

      <Table striped>
        { events.map((event) => (

          <StyledTableRow onClick={()=>{props.history.push('/event/view/'+ event.id)}}>
            <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>
              <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>
                <Image fluid src={(event.eventPicture && !event.eventPicture.includes('static')) ? 'http://' + event.eventPicture : eventPicture}/>
              </Grid.Column>
              <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>
                <Grid>
                  <Grid.Row columns={2}>
                    <Grid.Column textAlign='left' verticalAlign='middle'>
                      <List divided relaxed>
                        <List.Item>
                          <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>
                          <List.Content>
                            <List.Description><Trans>organisedBy</Trans></List.Description>
                            <List.Header>{event.organizationName}</List.Header>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon name='calendar alternate outline' size='large' color='red' verticalAlign='middle'/>
                          <List.Content>
                            <List.Description><Trans>date</Trans>:</List.Description>
                            <List.Header>{event.date}</List.Header>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon name='clock outline' size='large' color='red' verticalAlign='middle'/>
                          <List.Content>
                            <List.Description><Trans>hour</Trans>:</List.Description>
                            <List.Header>{event.time}</List.Header>
                          </List.Content>
                        </List.Item>
                      </List>
                    </Grid.Column>

                    <Grid.Column textAlign='left' verticalAlign='middle'>
                      <List divided relaxed>
                        <List.Item>
                          <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>
                          <List.Content>
                            <List.Description><Trans>location</Trans>:</List.Description>
                            <List.Header>{event.city}</List.Header>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>
                          <List.Content>
                            <List.Description> </List.Description>
                            <List.Header>{event.address}</List.Header>
                          </List.Content>
                        </List.Item>
                        <List.Item>
                          <List.Icon name='ticket' size='large' color='red' verticalAlign='middle'/>
                          <List.Content>
                            <List.Description><Trans>ticket</Trans>:</List.Description>
                            {event.tickets !== null && ((event.tickets.includes('www') > 0) ? (
                                (event.tickets.includes('http') > 0) ? (
                                  <List.Header><Item as={'a'} href={event.tickets} target="_blank">{event.tickets}</Item></List.Header>
                                ) : (
                                  <List.Header><Item as={'a'} href={'https://' + event.tickets} target="_blank">{event.tickets}</Item></List.Header>
                                )
                            ) : (
                              <List.Header>{event.tickets}</List.Header>
                            ))
                            }
                          </List.Content>
                        </List.Item>
                      </List>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>

              </Grid.Column>
            </Grid>
          </StyledTableRow>

        ))}

        {/*<StyledTableRow onClick={()=>{props.history.push('/event/view/1')}}>*/}
        {/*  <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>*/}
        {/*    <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>*/}
        {/*      <Image fluid src={eventPicture}/>*/}
        {/*    </Grid.Column>*/}
        {/*    <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>*/}
        {/*      <Grid>*/}
        {/*        <Grid.Row columns={2}>*/}
        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Organizat de:</List.Description>*/}
        {/*                  <List.Header>Super Kombat</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='calendar alternate outline' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Data:</List.Description>*/}
        {/*                  <List.Header>21/06/2020</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='clock outline' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>ora:</List.Description>*/}
        {/*                  <List.Header>19:00</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}

        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Locatie:</List.Description>*/}
        {/*                  <List.Header>Arad</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description> </List.Description>*/}
        {/*                  <List.Header>Sala Polivalenta</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='ticket' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Bilete:</List.Description>*/}
        {/*                  <List.Header>-</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*      </Grid>*/}

        {/*    </Grid.Column>*/}
        {/*  </Grid>*/}
        {/*</StyledTableRow>*/}


        {/*<StyledTableRow onClick={()=>{props.history.push('/event/view/2')}}>*/}
        {/*  <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>*/}
        {/*    <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>*/}
        {/*      <Image fluid src={eventPicture2}/>*/}
        {/*    </Grid.Column>*/}
        {/*    <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>*/}

        {/*      <Grid>*/}
        {/*        <Grid.Row columns={2}>*/}
        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Organizat de:</List.Description>*/}
        {/*                  <List.Header>Dynamite Fighting Show</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='calendar alternate outline' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Data:</List.Description>*/}
        {/*                  <List.Header>05/07/2020</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='clock outline' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>ora:</List.Description>*/}
        {/*                  <List.Header>20:00</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}

        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Locatie:</List.Description>*/}
        {/*                  <List.Header>Bucuresti</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description> </List.Description>*/}
        {/*                  <List.Header>Sala Polivalenta</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='ticket' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Bilete:</List.Description>*/}
        {/*                  <List.Header><Item as={'a'} href={'https://www.eventim.ro/ro/'} target="_blank">www.eventim.ro</Item></List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*      </Grid>*/}


        {/*    </Grid.Column>*/}
        {/*  </Grid>*/}
        {/*</StyledTableRow>*/}


        {/*<StyledTableRow onClick={()=>{props.history.push('/event/view/3')}}>*/}
        {/*  <Grid columns={2} style={{paddingTop: '1rem', paddingBottom: '1rem'}}>*/}
        {/*    <Grid.Column textAlign='center' verticalAlign='middle' mobile={16} tablet={7} computer={7}>*/}
        {/*      <Image fluid src={eventPicture}/>*/}
        {/*    </Grid.Column>*/}
        {/*    <Grid.Column textAlign='left' verticalAlign='middle' mobile={16} tablet={9} computer={9}>*/}

        {/*      <Grid>*/}
        {/*        <Grid.Row columns={2}>*/}
        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='trophy' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Organizat de:</List.Description>*/}
        {/*                  <List.Header>Super Kombat</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='calendar alternate outline' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Data:</List.Description>*/}
        {/*                  <List.Header>26/08/2020</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='clock outline' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>ora:</List.Description>*/}
        {/*                  <List.Header>19:30</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}

        {/*          <Grid.Column textAlign='left' verticalAlign='middle'>*/}
        {/*            <List divided relaxed>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Locatie:</List.Description>*/}
        {/*                  <List.Header>Bucuresti</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='location arrow' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description> </List.Description>*/}
        {/*                  <List.Header>Romexpo</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*              <List.Item>*/}
        {/*                <List.Icon name='ticket' size='large' color='red' verticalAlign='middle'/>*/}
        {/*                <List.Content>*/}
        {/*                  <List.Description>Bilete:</List.Description>*/}
        {/*                  <List.Header>0745635243</List.Header>*/}
        {/*                </List.Content>*/}
        {/*              </List.Item>*/}
        {/*            </List>*/}
        {/*          </Grid.Column>*/}
        {/*        </Grid.Row>*/}
        {/*      </Grid>*/}


        {/*    </Grid.Column>*/}
        {/*  </Grid>*/}
        {/*</StyledTableRow>*/}

      </Table>
    </Container>
  )
}
const StyledTableRow = styled(Table.Row)`
:hover {
background-color: gainsboro !important;
cursor: pointer;
}
`
const StyledSearch = styled(Search)`
padding-top: 1rem;
div:first-of-type {
width: 100%;
}
`

export default Events
