import React from 'react'
import {Button, Divider, Form, Grid, Header, Segment, Container, Item, Icon} from "semantic-ui-react";
import styled from "styled-components";
import EventForm from "../../../Components/EventForm";

const Event = (props) => {
  return (
    <Container>
      <EventForm {...props}/>
    </Container>
  )
}
export default Event
