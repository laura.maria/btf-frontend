import React from 'react'
import {Container, Grid, Image} from "semantic-ui-react"
import ProfileForm from '../../Components/ProfileForm'
import ProfilePictures from '../../Components/ProfilePictures'
import FightForm from "../../Components/FightForm";
import FightList from "../../Components/FightList";
import FighterRecords from "../../Components/FighterRecords";

const Records = (props) => {

  return (
    <Container>
      <Grid columns={2}>
        <Grid.Column mobile={16} tablet={6} computer={6}>
          <ProfilePictures {...props}/>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={10} computer={10}>
          <FightForm {...props}/>
          <FightList {...props}/>
          {/*<FighterRecords {...props}/>*/}
        </Grid.Column>
      </Grid>
    </Container>
  )
}

export default Records
